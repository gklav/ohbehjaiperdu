var nbObj;
var premObj;
var app = {


  // Application Constructor
  initialize: function() {
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    document.addEventListener("backbutton", this.afficherAccueil(), false);
  },

  // deviceready Event Handler
  //
  // Bind any cordova events here. Common events are:
  // 'pause', 'resume', etc.
  onDeviceReady: function() {
    //this.receivedEvent('deviceready');
    this.afficherAccueil();
  },


  //Lancement de la recherche et affichage des résultats
  affichageListe: function(){
    // Create a request variable and assign a new XMLHttpRequest object to it.
    var requete = new XMLHttpRequest();

    // Open a new connection, using the GET request on the URL endpoint
    requete.open('GET', 'https://data.sncf.com//api/records/1.0/search/?dataset=objets-trouves-restitution&rows='+nbObj+'&start='+premObj+'&sort=date&facet=gc_obo_date_heure_restitution_c&facet=gc_obo_gare_origine_r_name&facet=gc_obo_nature_c&facet=gc_obo_type_c&facet=gc_obo_nom_recordtype_sc_c ', true);

    requete.onload = function () {
      var donnees = JSON.parse(this.response);
      if (requete.status >= 200 && requete.status < 400) {
        for(var i=0; i<donnees.records.length; i++){
          var contenu = document.getElementById("liste");
          var carte = document.createElement("li");
          carte.setAttribute("class","table-view-cell media");

          carte.innerHTML = '<a class="navigate-right"><div class="media-body">'+donnees.records[i].fields.gc_obo_nature_c+'<p>'+donnees.records[i].fields.gc_obo_gare_origine_r_name+'</p><p>'+donnees.records[i].fields.gc_obo_type_c+'</p><p>'+donnees.records[i].fields.date+'</p></div></a>';
          //+donnees.records[i].recordid+
          contenu.appendChild(carte);
          carte.childNodes[0].setAttribute("onclick","app.afficherPageDetails('"+donnees.records[i].recordid+"')");
        }
      } else {
        console.log("Erreur");
      }
    }

    // Send request
    requete.send();
  },


chargerElement: function(){
    var scrollHeight = $(document).height();
    var scrollPosition = $(window).height() + $(window).scrollTop();
    if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
      premObj = premObj + nbObj;
      this.affichageListe();
    }
  },

  effacerEcran: function(){
    document.getElementById("app").innerHTML = '';
  },

  afficherAccueil: function(){
    this.effacerEcran();

    nbObj = 30;
    premObj = 0;

    var leApp = document.getElementById("app");
    var menu = document.createElement("header");
    menu.setAttribute("class","bar bar-nav");
    menu.innerHTML= '<h1 class="title" style="height: auto;">OhBehJaiPerdu!</h1>';

    var contenu = document.createElement("div");
    contenu.setAttribute("class","content");
    contenu.innerHTML = '<ul id="liste" class="tab-view" style="padding: 0;" ontouchmove="app.chargerElement();"/></ul>';

    leApp.appendChild(menu);
    leApp.appendChild(contenu);

    this.affichageListe();

  },

afficherPageDetails: function(id){
  console.log(6);
  this.effacerEcran();

  console.log(1);

  var leApp = document.getElementById("app");
  var menu = document.createElement("header");
  menu.setAttribute("class","bar bar-nav");
  menu.innerHTML= '<a class="icon icon-left-nav pull-left" onclick="app.afficherAccueil();"></a><h1 class="title" style="height: auto;">OhBehJaiPerdu!</h1>';

  var contenu = document.createElement("div");
  contenu.setAttribute("class","content");
  contenu.setAttribute("id","details");

  leApp.appendChild(menu);
  leApp.appendChild(contenu);

  this.afficherDetails();

  this.afficherDetails(id);
},

  afficherDetails: function(id){
    // Create a request variable and assign a new XMLHttpRequest object to it.
    var requete = new XMLHttpRequest();

    // Open a new connection, using the GET request on the URL endpoint
    requete.open('GET', 'https://data.sncf.com/api/records/1.0/search/?dataset=objets-trouves-restitution&q=recordid+%3D%22'+ id +'%22&facet=gc_obo_date_heure_restitution_c&facet=gc_obo_gare_origine_r_name&facet=gc_obo_nature_c&facet=gc_obo_type_c&facet=gc_obo_nom_recordtype_sc_c', true);

    requete.onload = function () {
      var donnees = JSON.parse(this.response);
      console.log(donnees);
      if (requete.status >= 200 && requete.status < 400) {
        for(var i=0; i<donnees.records.length; i++){
          var contenu = document.getElementById("details");
          //contenu.setAttribute("class","table-view-cell media");
          contenu.innerHTML = '<h2>'+donnees.records[i].fields.gc_obo_nature_c+'</h2><h3>Gare : '+donnees.records[i].fields.gc_obo_gare_origine_r_name+'</h3><h4>Catégorie : '+donnees.records[i].fields.gc_obo_type_c+'</h4>';
          contenu.innerHTML = contenu.innerHTML + '<h4>Date de restitution : </h4>' + donnees.records[i].fields.date;

          //contenu.appendChild(details);
        }

      } else {
        console.log("Erreur");
      }
    }

    // Send request
    requete.send();
  }

};

app.initialize();
